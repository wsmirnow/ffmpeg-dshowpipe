#include "dshowpipe.h"
#include <initguid.h>

#include <libavcodec/avcodec.h>
#include <libavcodec/raw.h>
#include <libavformat/avformat.h>
#include <libavformat/internal.h>
#include <libavformat/avio.h>
#include <libavutil/avassert.h>
#include <libavutil/opt.h>
#include <libavutil/timestamp.h>


//// aditional definietions ////

DEFINE_GUID(MEDIASUBTYPE_V210, 
        0x30313276, 0x0000, 0x0010, 
        0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

////////

typedef struct _PipePacketList {
    LPVOID pBuffer;
    SIZE_T cBuffer;
    struct _PipePacketList *next;
} PipePacketList;

typedef struct _DShowPipeDemuxerContext {
    AVClass *class;
    BOOL ds_frame_duration_fix;
    REFERENCE_TIME reft_last_start;
    REFERENCE_TIME reft_last_stop;
    int64_t lastPts;
    int lastDuration;
} DShowPipeDemuxerContext;

#define VALUE_AUTO -1

static const AVOption options[] = {
    { "ds_frame_duration_fix",
        "Option to fix video frame duration from DirectShow (diasble=0, enable=1, auto=-1)",
        offsetof(struct _DShowPipeDemuxerContext, ds_frame_duration_fix),
        AV_OPT_TYPE_INT, { .i64=VALUE_AUTO }, -1, 1, AV_OPT_FLAG_DECODING_PARAM },
    { NULL },
};

static const AVClass dshowpipe_demuxer_class = {
    .class_name = "dshowpipe",
    .item_name  = av_default_item_name,
    .option     = options,
    .version    = LIBAVUTIL_VERSION_INT,
    .category   = AV_CLASS_CATEGORY_DEMUXER,
};

static enum AVSampleFormat parse_sample_fmt(int bits_per_sample) {
    switch (bits_per_sample) {
        case 8:  return AV_SAMPLE_FMT_U8;
        case 16: return AV_SAMPLE_FMT_S16;
        case 32: return AV_SAMPLE_FMT_S32;
        default: return AV_SAMPLE_FMT_NONE;
    }
}

static enum AVCodecID parse_audio_codec_id(enum AVSampleFormat sample_fmt) {
    switch (sample_fmt) {
        case AV_SAMPLE_FMT_U8:  return AV_CODEC_ID_PCM_U8;
        case AV_SAMPLE_FMT_S16: return AV_CODEC_ID_PCM_S16LE;
        case AV_SAMPLE_FMT_S32: return AV_CODEC_ID_PCM_S32LE;
        default:                return AV_CODEC_ID_NONE;
    }
}

static enum AVPixelFormat parse_pix_fmt(DWORD biCompression, WORD biBitCount) {
    switch(biCompression) {
        case BI_RGB:
        case BI_BITFIELDS:
            switch(biBitCount) {
                case 1: return AV_PIX_FMT_MONOWHITE;
                case 4: return AV_PIX_FMT_RGB4;
                case 8: return AV_PIX_FMT_RGB8;
                case 16: return AV_PIX_FMT_RGB555;
                case 24: return AV_PIX_FMT_BGR24;
                case 32: return AV_PIX_FMT_RGB32;
                default: break;
            }
        default: break;
    }
    return avpriv_find_pix_fmt(ff_raw_pix_fmt_tags, biCompression);
}

static enum AVPixelFormat parse_yuv_pix_fmt(WORD biBitCount) {
    switch (biBitCount) {
    case 12: return AV_PIX_FMT_YUV420P;
    case 16: return AV_PIX_FMT_YUV422P;
    case 24: return AV_PIX_FMT_YUV444P;
    default: return AV_PIX_FMT_NONE;
    }
}

static int dshowpipe_get_packet_type(void *pBuffer, SIZE_T cBuffer) {
    if (cBuffer <= sizeof(PipePacketHeader))
        return AVERROR_INVALIDDATA;

    PipePacketHeader *pHeader = (PipePacketHeader *)pBuffer;
    return pHeader->type;
}

static int dshowpipe_read_raw_pipe_packet(AVIOContext *pb, void **ppBuffer, SIZE_T *pcBuffer) {
    PipePacketHeader header;

    av_assert0(pb != NULL);
    header.length = avio_rl32(pb);
    if (header.length == 0) return AVERROR_EOF;

    header.type = avio_rl32(pb);
    if (header.type == 0) return AVERROR_EOF;

    av_assert0(header.length > sizeof(PipePacketHeader));

    if (*ppBuffer) av_freep(ppBuffer);

    *pcBuffer = 0;
    *ppBuffer = av_mallocz(header.length);
    if (!*ppBuffer) return AVERROR(ENOMEM);

    *pcBuffer = header.length;
    memcpy(*ppBuffer, &header, sizeof(PipePacketHeader));
    if (avio_read(pb,
            (*ppBuffer) + sizeof(PipePacketHeader),
            header.length - sizeof(PipePacketHeader)) <= 0)
    {
        av_freep(ppBuffer);
        *pcBuffer = 0;
    }

    return *pcBuffer;
}

static int dshowpipe_parse_mediatype_pipe_packet(AVFormatContext *s, void *pBuffer, SIZE_T cBuffer) {
    PipePacketHeader *pHeader = NULL;
    PipePacketMediaType *pBody = NULL;
    AVStream *st = NULL;
    AVRational time_base = (AVRational) { 1, DSHOW_SECOND };
    DShowPipeDemuxerContext *c = s->priv_data;

    av_assert0(s != NULL);
    if (!pBuffer || cBuffer <= 0)
        return AVERROR(EACCES);

    if (cBuffer < sizeof(PipePacketHeader) + sizeof(PipePacketMediaType))
        return AVERROR_INVALIDDATA;

    pHeader = (PipePacketHeader *) pBuffer;
    if (pHeader->length != cBuffer ||
            (pHeader->type != PACKET_MEDIA_TYPE &&
            pHeader->type != PACKET_MEDIA_TYPE_EX))
        return AVERROR_INVALIDDATA;

    pBody = (PipePacketMediaType *) (pBuffer + sizeof(PipePacketHeader));
    st = avformat_new_stream(s, NULL);
    if (st == NULL)
        return AVERROR(ENOMEM);

    av_assert0(st->codec);

    if (IsEqualGUID(&MEDIATYPE_Audio, &pBody->majortype)) {
        av_log(s, AV_LOG_DEBUG, "audio stream found on %s\n", s->filename);
        if (IsEqualGUID(&MEDIASUBTYPE_PCM, &pBody->subtype)) {
            // support raw pcm stream
            if (!IsEqualGUID(&FORMAT_WaveFormatEx, &pBody->formattype))
                return AVERROR_DECODER_NOT_FOUND;
            if (pBody->cbFormat < sizeof(WAVEFORMATEX))
                return AVERROR_INVALIDDATA;

            WAVEFORMATEX *wf = (WAVEFORMATEX *) &pBody->pbFormat;
            av_assert0(wf->wFormatTag == WAVE_FORMAT_PCM ||
                    wf->wFormatTag == WAVE_FORMAT_EXTENSIBLE);

            // setup audio codec
            st->codec->codec_type = AVMEDIA_TYPE_AUDIO;
            st->codec->sample_fmt = parse_sample_fmt(wf->wBitsPerSample);
            st->codec->codec_id = parse_audio_codec_id(st->codec->sample_fmt);
            st->codec->channels = wf->nChannels;
            st->codec->sample_rate = wf->nSamplesPerSec;
            //time_base = (AVRational) { 1, st->codec->sample_rate };


        } else {
            av_log(s, AV_LOG_ERROR, "we doesn't support encoded audio streams\n");
            return AVERROR_DECODER_NOT_FOUND;
        }

        if (c->ds_frame_duration_fix == VALUE_AUTO) {
            c->ds_frame_duration_fix = FALSE;
            av_log(s, AV_LOG_DEBUG, "set ds_frame_duration_fix to %d on %s\n",
                c->ds_frame_duration_fix, s->filename);
        }
    } else if (IsEqualGUID(&MEDIATYPE_Video, &pBody->majortype)) {
        av_log(s, AV_LOG_DEBUG, "video stream found on %s\n", s->filename);

        if (IsEqualGUID(&FORMAT_VideoInfo, &pBody->formattype) ||
                IsEqualGUID(&FORMAT_VideoInfo2, &pBody->formattype)) {

            st->codec->codec_type = AVMEDIA_TYPE_VIDEO;

            BITMAPINFOHEADER *bih = NULL;
            if (IsEqualGUID(&FORMAT_VideoInfo, &pBody->formattype)) {
                if (pBody->cbFormat < sizeof(VIDEOINFOHEADER))
                    return AVERROR_INVALIDDATA;

                VIDEOINFOHEADER *vih = (VIDEOINFOHEADER *) &pBody->pbFormat;
                bih = &vih->bmiHeader;
                st->codec->bit_rate = vih->dwBitRate;
                st->avg_frame_rate = (AVRational) { DSHOW_SECOND, vih->AvgTimePerFrame };
            } else {
                if (pBody->cbFormat < sizeof(VIDEOINFOHEADER2))
                    return AVERROR_INVALIDDATA;

                VIDEOINFOHEADER2 *vih2 = (VIDEOINFOHEADER2 *) &pBody->pbFormat;
                bih = &vih2->bmiHeader;
                st->codec->bit_rate = vih2->dwBitRate;
                st->avg_frame_rate = (AVRational) { DSHOW_SECOND, vih2->AvgTimePerFrame };
            }

            st->codec->width = bih->biWidth;
            st->codec->height = bih->biHeight;
            st->codec->pix_fmt = parse_pix_fmt(bih->biCompression, bih->biBitCount);

            if (st->codec->pix_fmt == AV_PIX_FMT_NONE) {
                if (IsEqualGUID(&MEDIASUBTYPE_MJPG, &pBody->subtype)) {
                    st->codec->codec_id = AV_CODEC_ID_MJPEG;
                    st->codec->pix_fmt = parse_yuv_pix_fmt(bih->biBitCount);
                    if (st->codec->pix_fmt == AV_PIX_FMT_NONE) {
                        av_log(s, AV_LOG_ERROR, "failed to parse pixel format for an MJPG video stream\n");
                        return AVERROR_INVALIDDATA;
                    }

                } else if (IsEqualGUID(&MEDIASUBTYPE_DVSD, &pBody->subtype) ||
                        IsEqualGUID(&MEDIASUBTYPE_dvsd, &pBody->subtype)) {
                    st->codec->codec_id = AV_CODEC_ID_DVVIDEO;
                    st->codec->pix_fmt = parse_yuv_pix_fmt(bih->biBitCount);
                    if (st->codec->pix_fmt == AV_PIX_FMT_NONE) {
                        av_log(s, AV_LOG_ERROR, "failed to parse pixel format for a digital video stream\n");
                        return AVERROR_INVALIDDATA;
                    }
                } else if (IsEqualGUID(&MEDIASUBTYPE_V210, &pBody->subtype)) {
                    st->codec->codec_id = AV_CODEC_ID_V210;
                    st->codec->pix_fmt = AV_PIX_FMT_YUV422P10LE;
                } else {
                    // pasring pixel format failed
                    // TODO: fix me
                    av_log(s, AV_LOG_ERROR, "failed to parse pixel format\n");
                    return AVERROR_INVALIDDATA;
                }
            } else {
                st->codec->codec_id = AV_CODEC_ID_RAWVIDEO;
                if (bih->biCompression == BI_RGB || bih->biCompression == BI_BITFIELDS) {
                    if (st->codec->height >= 0) {
                        // DirectShow RGB framebuffer is ButtomUp if height value is positive
                        st->codec->extradata = av_malloc(9 + FF_INPUT_BUFFER_PADDING_SIZE);
                        if (st->codec->extradata) {
                            st->codec->extradata_size = 9;
                            memcpy(st->codec->extradata, "BottomUp", 9);
                        }
                    }
                }
            }

            if (c->ds_frame_duration_fix == VALUE_AUTO) {
                if (st->codec->codec_id == AV_CODEC_ID_DVVIDEO) {
                    c->ds_frame_duration_fix = FALSE;
                } else {
                    c->ds_frame_duration_fix = TRUE;
                }

                av_log(s, AV_LOG_DEBUG, "set ds_frame_duration_fix to %d on %s\n",
                    c->ds_frame_duration_fix, s->filename);
            }
        }
    } else {
        LPOLESTR guid_str[GUID_CCH];
        StringFromGUID2(&pBody->majortype, guid_str, GUID_CCH);
        av_log(s, AV_LOG_DEBUG, "unknown media type: %s\n", *guid_str);
        return AVERROR_INVALIDDATA;
    }

    // set stream time_base
    avpriv_set_pts_info(st, 64, time_base.num, time_base.den);
    return 0;
}

static BOOL dshowpipe_check_pipe_packet_drop(
                        AVFormatContext *s,
                        REFERENCE_TIME reft_start) {
    BOOL sample_drop = FALSE;
    DShowPipeDemuxerContext *c = NULL;
    REFERENCE_TIME reft_sample_length = 0;

    av_assert0(s != NULL);
    c = (DShowPipeDemuxerContext *) s->priv_data;
    av_assert0(c != NULL);
    if (c->reft_last_start && c->reft_last_start < reft_start) {
        if (c->reft_last_stop) {
            reft_sample_length = c->reft_last_stop - c->reft_last_start;
            if (c->reft_last_stop + reft_sample_length <= reft_start) {
                sample_drop = TRUE;
            }
        } else if (s->nb_streams) {
            av_assert0(s->streams != NULL);
            if (s->streams[0]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
                // TODO: determine audio sample drop without preset sample length
            } else if (s->streams[0]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
                if ((reft_start - c->reft_last_start) / s->streams[0]->avg_frame_rate.den > 1) {
                    sample_drop = TRUE;
                }
            }
        }
    }
    
    return sample_drop;
}

static int dshowpipe_parse_data_pipe_packet(AVFormatContext *s, AVPacket *pkt, void *pBuffer, SIZE_T cBuffer) {
    int ret = 0;
    DShowPipeDemuxerContext *c = NULL;

    av_assert0(s != NULL);
    c = (DShowPipeDemuxerContext *) s->priv_data;
    if(!pBuffer || cBuffer < sizeof(PipePacketHeader) + sizeof(PipePacketDataBody))
        return AVERROR_INVALIDDATA;

     PipePacketHeader *pHeader = (PipePacketHeader *) pBuffer;
     PipePacketDataBody *pData = (PipePacketDataBody *) (pBuffer + sizeof(PipePacketHeader));

     if (pHeader->length != cBuffer ||
             pData->cbBuffer >= cBuffer - sizeof(PipePacketHeader))
         return AVERROR_INVALIDDATA;

     // create new packet
    ret = av_new_packet(pkt, pData->cbBuffer);
    if (ret != 0)
        return ret;

    av_log(s, AV_LOG_DEBUG,
            "pipe packet recieved from %s [%lld - %lld] ~%lldms, media: [%lld - %lld] size: %ld\n",
            s->filename,
            pData->tStart,
            pData->tStop,
            pData->tStart != 0 && pData->tStop != 0 ?
                (pData->tStop - pData->tStart) / DSHOW_MILLISECOND : 0,
            pData->tMediaStart,
            pData->tMediaStop,
            pData->cbBuffer);

    av_assert0(pkt->size >= pData->cbBuffer);

    pkt->stream_index = 0;
    memcpy(pkt->data, &pData->pBuffer, pData->cbBuffer);
    if (av_cmp_q(av_make_q(1, DSHOW_SECOND), s->streams[0]->time_base) != 0 && av_q2d(s->streams[0]->time_base) != 0) {
        LONGLONG ds_duration = pData->tStop - pData->tStart;
        pkt->pts = (DOUBLE)((DOUBLE)pData->tStart / (DOUBLE)DSHOW_SECOND / av_q2d(s->streams[0]->time_base));
        pkt->duration = (DOUBLE)((DOUBLE)ds_duration / (DOUBLE)DSHOW_SECOND / av_q2d(s->streams[0]->time_base));
    } else {
        pkt->pts = pData->tStart > 0 ? pData->tStart : AV_NOPTS_VALUE;
        pkt->duration = pData->tStop > pData->tStart ? pData->tStop - pData->tStart : 0;
    }
    if (c->ds_frame_duration_fix == TRUE)
        pkt->duration = 0; // unset packet duration, because DirectShow video timestamps are inaccurate

    if (dshowpipe_check_pipe_packet_drop(s, pData->tStart)) {
        if (c->reft_last_stop && pData->tStart && c->reft_last_stop < pData->tStart) {
            av_log(s, AV_LOG_WARNING,
                "lost sample on \"%s\" (%ssec - %ssec ~%.3fsec)\n",
                s->filename,
                av_ts2timestr(c->reft_last_stop - s->streams[0]->start_time, &s->streams[0]->time_base),
                av_ts2timestr(pData->tStart - s->streams[0]->start_time, &s->streams[0]->time_base),
                (DOUBLE)(pData->tStart - c->reft_last_stop) / DSHOW_SECOND);
        } else {
            av_log(s, AV_LOG_WARNING,
            "lost sample on \"%s\"",
            s->filename);
        }
    }

    c->reft_last_start = pData->tStart;
    c->reft_last_stop = pData->tStop;

//    av_log(s, AV_LOG_INFO,
//        "%s pts: %s duration: %s\n",
//        s->filename,
//        av_ts2timestr(pkt->pts - s->streams[0]->start_time, &s->streams[0]->time_base),
//        av_ts2timestr(pkt->duration, &s->streams[0]->time_base));

    return 0;
}

static int dshowpipe_read_header(AVFormatContext *s) {
    int ret = 0;
    void *pBuffer = NULL;
    SIZE_T cBuffer = 0;
    DShowPipeDemuxerContext *c = s->priv_data;

    c->reft_last_start = c->reft_last_stop = 0LL;
    c->lastPts = c->lastDuration = 0;
    ret = dshowpipe_read_raw_pipe_packet(s->pb, &pBuffer, &cBuffer);
    if (ret <= 0)
        return ret == 0 ? AVERROR_EOF : ret;

    av_assert0(cBuffer > 0);
    av_assert0(pBuffer != NULL);

    ret = dshowpipe_parse_mediatype_pipe_packet(s, pBuffer, cBuffer);
    av_freep(&pBuffer);

    return 0;
}

static int dshowpipe_read_packet(AVFormatContext *s, AVPacket *pkt) {
    int ret = 0;
    DShowPipeDemuxerContext *c = NULL;
    PipePacketList *pPipePkt = NULL;

    av_assert0(s != NULL);
    av_assert0(s->priv_data != NULL);
    c = s->priv_data;

    pPipePkt = av_mallocz(sizeof(PipePacketList));
    if (!pPipePkt) return AVERROR(ENOMEM);

    ret = dshowpipe_read_raw_pipe_packet(s->pb,
            &pPipePkt->pBuffer, &pPipePkt->cBuffer);
    if (ret == 0) ret = AVERROR_EOF;
    if (ret < 0) goto done;

    av_assert0(pPipePkt->cBuffer > 0);
    av_assert0(pPipePkt->pBuffer != NULL);

    switch (dshowpipe_get_packet_type(pPipePkt->pBuffer, pPipePkt->cBuffer)) {
        case PACKET_DATA_TYPE:
            ret = dshowpipe_parse_data_pipe_packet(s, pkt,
                    pPipePkt->pBuffer, pPipePkt->cBuffer);
            break;
        // add other PipePacket type handler to handle stream correctly
        default:
            // does not support dynamic mediatype changes
            av_log(s, AV_LOG_ERROR, "dynamic media type changes not supported\n");
            ret = AVERROR_INVALIDDATA;
    }

done:
    if (pPipePkt) {
        if (pPipePkt->pBuffer) {
            av_free(pPipePkt->pBuffer);
        }
        av_free(pPipePkt);
    }
    if (ret < 0 && pkt)
        av_free_packet(pkt);
    return ret;
}

static int dshowpipe_read_close(AVFormatContext *s) {
    av_log(s, AV_LOG_DEBUG, "dshowpipe_read_close called\n");

    return 0;
}

AVInputFormat ff_dshowpipe_demuxer = {
    .name           = "dshowpipe",
    .long_name      = NULL_IF_CONFIG_SMALL("DirectShow input over named pipe"),
    .priv_data_size = sizeof(DShowPipeDemuxerContext),
    .read_header    = dshowpipe_read_header,
    .read_packet    = dshowpipe_read_packet,
    .read_close     = dshowpipe_read_close,
    .priv_class     = &dshowpipe_demuxer_class,
};
