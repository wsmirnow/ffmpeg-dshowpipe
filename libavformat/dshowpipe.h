
#ifndef DSHOWPIPE_H
#define	DSHOWPIPE_H

#define COBJMACROS
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#define NO_DSHOW_STRSAFE
#include <dshow.h>
#include <dvdmedia.h>


#define WAVE_FORMAT_PCM 1
#define WAVE_FORMAT_EXTENSIBLE 0xFFFE

#define DSHOW_SECOND 10000000
#define DSHOW_MILLISECOND 10000

#define GUID_CCH 39

////////////////////////////////////////////////////////////////////////////////
// DShow Pipe structures

#define PIPE_NAME_PREFIX "\\\\.\\pipe\\"

#define PACKET_MEDIA_TYPE 1
#define PACKET_MEDIA_TYPE_EX 3
#define PACKET_DATA_TYPE 2

typedef struct _PipePacketHeader {
  DWORD length;
  DWORD type;
} PipePacketHeader;

typedef struct _PipePacketMediaType {
  GUID     majortype;
  GUID     subtype;
  BOOL     bFixedSizeSamples;
  BOOL     bTemporalCompression;
  ULONG    lSampleSize;
  GUID     formattype;
  ULONG    cbFormat;
  BYTE     *pbFormat;
} PipePacketMediaType;

typedef struct _PipePacketMediaTypeEx {
  PipePacketMediaType mediaType;
  ALLOCATOR_PROPERTIES allocProps;
} PipePacketMediaTypeEx;

typedef struct _PipePacketDataBody {
  LONGLONG tStart;
  LONGLONG tStop;
  LONGLONG tMediaStart;
  LONGLONG tMediaStop;
  DWORD bSync;
  LONG cbBuffer;
  LPBYTE pBuffer;
} PipePacketDataBody;

#endif	/* DSHOWPIPE_H */

